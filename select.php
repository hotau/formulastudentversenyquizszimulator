﻿<?php  
 include'connect.php';
 
 $sql="SET @count = 0"; 
	$result=$connect->query($sql);
	$sql="UPDATE questions SET id = @count:= @count + 1"; 
	$result=$connect->query($sql);
	$sql="ALTER TABLE questions AUTO_INCREMENT = 1"; 
	$result=$connect->query($sql);
 
 $output = '';  
 $sql = "SELECT * FROM questions ORDER BY id DESC";  
 $result = mysqli_query($connect, $sql);
 $output .= '  
      <div class="table-responsive">
		<button><a href="lekerdezes.php">Játék</a></button>  
           <table class="table table-bordered">  
                <tr>  
                     <th width="5%">Id</th>  
                     <th width="15%">Kérdés</th>  
                     <th width="15%">Válasz 1</th>
					 <th width="15%">Válasz 2</th>
					 <th width="15%">Válasz 3</th>
					 <th width="15%">Válasz 4</th>
					 <th width="15%">Jó válasz</th>
                     <th width="5%">Delete</th>  
                </tr>';  
 $rows = mysqli_num_rows($result);
 if($rows > 0)  
 {  
      while($row = mysqli_fetch_array($result))  
      {  
           $output .= '  
                <tr>  
                     <td>'.$row["id"].'</td>  
                     <td class="kerdes" data-id1="'.$row["id"].'" contenteditable>'.$row["kerdes"].'</td>  
                     <td class="valasz1" data-id2="'.$row["id"].'" contenteditable>'.$row["valasz1"].'</td>
					 <td class="valasz2" data-id3="'.$row["id"].'" contenteditable>'.$row["valasz2"].'</td>
					 <td class="valasz3" data-id4="'.$row["id"].'" contenteditable>'.$row["valasz3"].'</td>
					 <td class="valasz4" data-id5="'.$row["id"].'" contenteditable>'.$row["valasz4"].'</td>
					 <td class="jovalasz" data-id6="'.$row["id"].'" contenteditable>'.$row["jovalasz"].'</td> 
                     <td><button type="button" name="delete_btn" data-id7="'.$row["id"].'" class="btn btn-xs btn-danger btn_delete">x</button></td>  
                </tr>  
           ';  
      }  
      $output .= '  
           <tr>  
                <td></td>  
                <td id="kerdes" contenteditable></td>  
                <td id="valasz1" contenteditable></td>
				<td id="valasz2" contenteditable></td>
				<td id="valasz3" contenteditable></td>
				<td id="valasz4" contenteditable></td>
				<td id="jovalasz" contenteditable></td> 
                <td><button type="button" name="btn_add" id="btn_add" class="btn btn-xs btn-success">+</button></td>  
           </tr>  
      ';  
 }  
 else  
 {  
      $output .= '
				<tr>  
					<td></td>  
					<td id="kerdes" contenteditable></td>  
					<td id="valasz1" contenteditable></td>
					<td id="valasz2" contenteditable></td>
					<td id="valasz3" contenteditable></td>
					<td id="valasz4" contenteditable></td>
					<td id="jovalasz" contenteditable></td> 
					<td><button type="button" name="btn_add" id="btn_add" class="btn btn-xs btn-success">+</button></td>  
			   </tr>';  
 }  
 $output .= '</table>  
      </div>';  
 echo $output;
 
 
	

 
 ?>