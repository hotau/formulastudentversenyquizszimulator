-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jul 01, 2019 at 09:03 AM
-- Server version: 10.1.38-MariaDB
-- PHP Version: 7.3.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `questions`
--

-- --------------------------------------------------------

--
-- Table structure for table `questions`
--

CREATE TABLE `questions` (
  `id` int(10) UNSIGNED NOT NULL,
  `kerdes` varchar(100) NOT NULL DEFAULT '',
  `valasz1` varchar(100) NOT NULL DEFAULT '',
  `valasz2` varchar(100) NOT NULL DEFAULT '',
  `valasz3` varchar(100) NOT NULL DEFAULT '',
  `valasz4` varchar(100) NOT NULL DEFAULT '',
  `jovalasz` varchar(100) NOT NULL DEFAULT ''
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `questions`
--

INSERT INTO `questions` (`id`, `kerdes`, `valasz1`, `valasz2`, `valasz3`, `valasz4`, `jovalasz`) VALUES
(1, 'Ez egy kerdes', 'valasz1', 'valasz2', 'Jo valasz', 'valasz4', 'Jo valasz'),
(2, 'Ez egy kerdes', 'valasz1', 'valasz2', 'Jo valasz', 'valasz4', 'Jo valasz'),
(3, 'Ez egy kerdes', 'valasz1', 'valasz2', 'Jo valasz', 'valasz4', 'Jo valasz'),
(4, 'Ez egy kerdes', 'valasz1', 'valasz2', 'Jo valasz', 'valasz4', 'Jo valasz'),
(5, 'Ez egy kerdes', 'valasz1', 'valasz2', 'Jo valasz', 'valasz4', 'Jo valasz'),
(6, 'Ez egy kerdes', 'valasz1', 'valasz2', 'Jo valasz', 'valasz4', 'Jo valasz'),
(7, 'Ez egy kerdes', 'valasz1', 'valasz2', 'Jo valasz', 'valasz4', 'Jo valasz'),
(8, 'Ez egy kerdes', 'valasz1', 'valasz2', 'Jo valasz', 'valasz4', 'Jo valasz'),
(9, 'Ez egy kerdes', 'valasz1', 'valasz2', 'Jo valasz', 'valasz4', 'Jo valasz'),
(10, 'Ez egy kerdes', 'valasz1', 'valasz2', 'Jo valasz', 'valasz4', 'Jo valasz'),
(11, 'Ez egy kerdes', 'valasz1', 'valasz2', 'Jo valasz', 'valasz4', 'Jo valasz'),
(12, 'Ez egy kerdes', 'valasz1', 'valasz2', 'Jo valasz', 'valasz4', 'Jo valasz'),
(13, 'Ez egy kerdes', 'valasz1', 'valasz2', 'Jo valasz', 'valasz4', 'Jo valasz'),
(14, 'Ez egy kerdes', 'valasz1', 'valasz2', 'Jo valasz', 'valasz4', 'Jo valasz');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `questions`
--
ALTER TABLE `questions`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id` (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `questions`
--
ALTER TABLE `questions`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
