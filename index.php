

<html lang="hu">  
    <head>  
        <title>kerdesek</title>  
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" />  
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>  
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script>  
    </head>  
    <body>  
        <div class="container">  
            <br />  
            <br />
			<br />
			<div class="table-responsive">  
				<span id="result"></span>
				<div id="live_data"></div>                 
			</div>  
		</div>
    </body>  
</html>  
<script>  
$(document).ready(function(){  
    function fetch_data()  
    {  
        $.ajax({  
            url:"select.php",  
            method:"POST",  
            success:function(data){  
				$('#live_data').html(data);  
            }  
        });  
    }  
    fetch_data();  
    $(document).on('click', '#btn_add', function(){  
        var kerdes = $('#kerdes').text();  
        var valasz1 = $('#valasz1').text();
		var valasz2 = $('#valasz2').text();
		var valasz3 = $('#valasz3').text();
		var valasz4 = $('#valasz4').text();
		var jovalasz = $('#jovalasz').text();
		
        if(kerdes == '')  
        {  
            alert("Írd be a kérdést");  
            return false;  
        }
		
        if(valasz1 == '')  
        {  
            alert("Írd be az első válasz lehetőséget");  
            return false;  
        }
		
		if(valasz2 == '')  
        {  
            alert("Írd be az második válasz lehetőséget");  
            return false;  
        }
		
		if(valasz3 == '')  
        {  
            alert("Írd be az harmadik válasz lehetőséget");  
            return false;  
        }
		
		if(valasz4 == '')  
        {  
            alert("Írd be az negyedik válasz lehetőséget");  
            return false;  
        }
		
		if(jovalasz == '')  
        {  
            alert("Írd be az jó válasz lehetőséget");  
            return false;  
        }
		
		
        $.ajax({  
            url:"insert.php",  
            method:"POST",  
            data:{
				
				kerdes:kerdes,
				valasz1:valasz1,
				valasz2:valasz2,
				valasz3:valasz3,
				valasz4:valasz4,
				jovalasz:jovalasz,
				},  
            dataType:"text",  
            success:function(data)  
            {  
                alert(data);  
                fetch_data();  
            }  
        })  
    });  
    
	function edit_data(id, text, column_name)  
    {  
        $.ajax({  
            url:"edit.php",  
            method:"POST",  
            data:{id:id, text:text, column_name:column_name},  
            dataType:"text",  
            success:function(data){  
                //alert(data);
				$('#result').html("<div class='alert alert-success'>"+data+"</div>");
            }  
        });  
    }  
    $(document).on('blur', '.kerdes', function(){  
        var id = $(this).data("id1");  
        var kerdes = $(this).text();  
        edit_data(id, kerdes, "kerdes");  
    });
	
    $(document).on('blur', '.valasz1', function(){  
        var id = $(this).data("id2");  
        var valasz1 = $(this).text();  
        edit_data(id,valasz1, "valasz1");  
    });
	
	$(document).on('blur', '.valasz2', function(){  
        var id = $(this).data("id3");  
        var valasz2 = $(this).text();  
        edit_data(id,valasz2, "valasz2");  
    });
	
	$(document).on('blur', '.valasz3', function(){  
        var id = $(this).data("id4");  
        var valasz3 = $(this).text();  
        edit_data(id,valasz3, "valasz3");  
    });
	
	$(document).on('blur', '.valasz4', function(){  
        var id = $(this).data("id5");  
        var valasz4 = $(this).text();  
        edit_data(id,valasz4, "valasz4");  
    });
	
	$(document).on('blur', '.jovalasz', function(){  
        var id = $(this).data("id6");  
        var jovalasz = $(this).text();  
        edit_data(id,jovalasz, "jovalasz");  
    });
	
    $(document).on('click', '.btn_delete', function(){  
        var id=$(this).data("id7");  
        if(confirm("Are you sure you want to delete this?"))  
        {  
            $.ajax({  
                url:"delete.php",  
                method:"POST",  
                data:{id:id},  
                dataType:"text",  
                success:function(data){  
                    alert(data);  
                    fetch_data();  
                }  
            });  
        }  
    });  
});  
</script>